angular.module("app.home")
    .controller("homeController", function(){
        var vm = this;
        
        vm.todos = [
            {
                id: 1,
                title: "Todo 1",
                description: "Todo 1",
                resolve: false,
                createdDate: new Date(),
                modifiedDate: null
            },
            {
                id: 2,
                title: "Todo 2",
                description: "Todo 2",
                resolve: false,
                createdDate: new Date(),
                modifiedDate: null
            },
            {
                id: 3,
                title: "Todo 3",
                description: "Todo 3",
                resolve: false,
                createdDate: new Date(),
                modifiedDate: null
            },            
        ];
        
    }]);