const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

let router = express.Router();

let todos = [{
        id: 1,
        title: "Todo 1",
        description: "Todo 1",
        resolve: false,
        createdDate: new Date(),
        modifiedDate: null
    },
    {
        id: 2,
        title: "Todo 2",
        description: "Todo 2",
        resolve: false,
        createdDate: new Date(),
        modifiedDate: null
    },
    {
        id: 3,
        title: "Todo 3",
        description: "Todo 3",
        resolve: true,
        createdDate: new Date(),
        modifiedDate: null
    }];

router.get('/todos', function (req, res) {
    res.json(todos);
});

router.get('/todos/:id', function (req, res) {
    let todo = null;
    let id = req.params.id;
    let index = 0
    
    for (let i = index; i < todos.length; i++){
        if (todos[i].id == id){
            todo = todos[i];
            i = todos.length;
        }
    }
    
    if (todo){
        res.status(200);
        res.json(todo);            
    }
    else{
        res.status(404)
            
            .send();
    }
});

router.post('/todos', function (req, res) {
    let todo = req.body;
    let lastId = todos[todos.length-1].id;
    
    todo.id = lastId + 1;
    todo.createdDate = new Date();
    todos.push(todo);
    
    res.status(201);
    res.json(todo);
});

router.put('/todos/:id', function (req, res) {
    let todo = req.body;
    let id = req.params.id;
    let index = 0
    
    for (let i = index; i < todos.length; i++){
        if (todos[i].id == id){
            index = i;
            i = todos.length;
        }
    }
    
    todo.id = todos[index].id;
    todo.modifiedDate = new Date();
    todos[index] = todo;
    
    res.status(200);
    res.json(todo);    
});

router.delete('/todos/:id', function (req, res) {
    let _todos = [];
    let _todo = null;
    let id = req.params.id;
    let index = 0
    
    for (let i = index; i < todos.length; i++){
        if (todos[i].id != id){
            _todos.push(todos[i]);   
        }else{
            _todo = todos[i];
        }
    }
    
    todos = _todos;
    
    res.status(200);
    res.json(_todo);
});

app.use(express.static('public'));
app.use('/api', router);

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});